[logo]: docs/logo.png

![University of San Carlos][logo]

## About
A voting web application for the Senior High School of the University of San Carlos.

The application will be launched in September 2018.

## License

The web application is licensed under the [MIT license](LICENSE).
