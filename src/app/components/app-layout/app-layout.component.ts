import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs/index';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-layout',
  templateUrl: './app-layout.component.html',
  styleUrls: [ './app-layout.component.scss' ]
})
export class AppLayoutComponent implements OnInit {
  @Input() drawerOpened: boolean;
  @Input() title: string;
  @Output() toolbarMenuClick = new EventEmitter();
  @Output() drawerClose = new EventEmitter();
  @Output() drawerOpen = new EventEmitter();

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver) { }

  ngOnInit() {
    this.isHandset$.subscribe(value => {
      this.drawerOpened = !value;
      if (this.drawerOpened) {
        this.drawerOpen.emit();
      } else {
        this.drawerClose.emit();
      }
    });
  }
}
