import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './app-toolbar.component.html',
  styleUrls: [ './app-toolbar.component.scss' ]
})
export class AppToolbarComponent {
  @Input() label: string;
  @Output() toolbarMenuClick = new EventEmitter();
}
