import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './routing';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppLayoutComponent } from './components/app-layout/app-layout.component';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatGridListModule,
  MatCardModule,
  MatMenuModule
} from '@angular/material';
import { AppDashboardComponent } from './components/app-dashboard/app-dashboard.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AppStoreModule } from './store';
import { AppToolbarComponent } from './components/app-toolbar/app-toolbar.component';

@NgModule({
  declarations: [
    AppComponent,
    AppLayoutComponent,
    AppDashboardComponent,
    AppToolbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    AppStoreModule
  ],
  providers: [],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
