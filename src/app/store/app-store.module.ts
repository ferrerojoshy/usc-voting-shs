import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { RouterStateSerializer, StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { LayoutStoreModule } from './layout';
import { RouterStoreModule, RouterStoreState } from './router';
import { environment } from '../../environments/environment';

@NgModule({
  imports: [
    CommonModule,
    LayoutStoreModule,
    RouterStoreModule,
    StoreModule.forRoot({}),
    StoreRouterConnectingModule.forRoot({
      stateKey: 'router', // name of reducer key
    }),
    // Instrument on development
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production // Log only mode in production
    }),
    EffectsModule.forRoot([])
  ],
  declarations: [],
  providers: [{ provide: RouterStateSerializer, useClass: RouterStoreState.Serializer }],
})
export class AppStoreModule { }
