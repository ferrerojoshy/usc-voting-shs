import {
  routerReducer as featureReducer,
} from '@ngrx/router-store';

export { featureReducer };
