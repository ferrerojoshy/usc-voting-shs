import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';

import { State } from './state';
import { Data } from '@angular/router';

const getData = (state: State) => state && state.state.data;

const getTitle = (state: State) => state && state.state.data.title;

export const selectRouteState: MemoizedSelector<object, State> = createFeatureSelector<State>('router');

export const selectRouteData: MemoizedSelector<object, Data> = createSelector(selectRouteState, getData);

export const selectRouteTitle: MemoizedSelector<object, string> = createSelector(selectRouteState, getTitle);
