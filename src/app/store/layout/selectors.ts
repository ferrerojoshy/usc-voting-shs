import {
  createFeatureSelector,
  createSelector,
  MemoizedSelector
} from '@ngrx/store';

import { State } from './state';

const getIsDrawerOpened = (state: State): boolean => state.isDrawerOpened;

export const selectLayoutState: MemoizedSelector<object, State> = createFeatureSelector<State>('layout');

export const selectLayoutIsDrawerOpened: MemoizedSelector<object, boolean> = createSelector(selectLayoutState, getIsDrawerOpened);
