import { Actions, ActionTypes } from './actions';
import { initialState, State } from './state';

export function featureReducer(state = initialState, action: Actions): State {
  switch (action.type) {
    case ActionTypes.CloseNavigation:
      return {
        ... state, isDrawerOpened: false
      };
    case ActionTypes.OpenNavigation:
      return {
        ... state, isDrawerOpened: true
      };
    case ActionTypes.ToggleNavigation:
      return {
        ... state, isDrawerOpened: !state.isDrawerOpened
      };
    default:
      return state;
  }
}
