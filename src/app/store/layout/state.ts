export interface State {
  isDrawerOpened: boolean;
}

export const initialState: State = {
  isDrawerOpened: true,
};
