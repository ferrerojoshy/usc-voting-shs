import { Action } from '@ngrx/store';

export enum ActionTypes {
  OpenNavigation = '[Layout] Open Navigation',
  CloseNavigation = '[Layout] Close Navigation',
  ToggleNavigation = '[Layout] Toggle Navigation'
}

export class OpenNavigation implements Action {
  readonly type = ActionTypes.OpenNavigation;
}

export class CloseNavigation implements Action {
  readonly type = ActionTypes.CloseNavigation;
}

export class ToggleNavigation implements Action {
  readonly type = ActionTypes.ToggleNavigation;
}

export type Actions = OpenNavigation | CloseNavigation | ToggleNavigation;
