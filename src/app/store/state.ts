import { LayoutStoreState } from './layout';
import { RouterStoreState } from './router';

export interface State {
  layout: LayoutStoreState.State;
  router: RouterStoreState.State;
}
