import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Store } from '@ngrx/store';

import {
  AppStoreState,
  LayoutStoreActions,
  LayoutStoreSelectors,
  RouterStoreSelectors
} from './store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit {
  title$: Observable<string>;
  isDrawerOpened$: Observable<boolean>;

  constructor(private store$: Store<AppStoreState.State>) { }

  ngOnInit() {
    this.title$ = this.store$.select(
      RouterStoreSelectors.selectRouteTitle
    );
    this.isDrawerOpened$ = this.store$.select(
      LayoutStoreSelectors.selectLayoutIsDrawerOpened
    );
  }

  onToolbarMenuClick() {
    this.store$.dispatch(
      new LayoutStoreActions.ToggleNavigation()
    );
  }

  onDrawerClose() {
    this.store$.dispatch(
      new LayoutStoreActions.CloseNavigation()
    );
  }

  onDrawerOpen() {
    this.store$.dispatch(
      new LayoutStoreActions.OpenNavigation()
    );
  }
}
