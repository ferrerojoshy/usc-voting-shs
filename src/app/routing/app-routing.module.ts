import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppDashboardComponent } from '../components/app-dashboard/app-dashboard.component';
import * as AppRoutes from './routes';

const routes: Routes = [
  {
    path: AppRoutes.Index,
    component: AppDashboardComponent,
    data: {
      title: 'Dashboard'
    }
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
